class Player:
    def __init__(self, armyName, location, obj = None):  #Initializes obj as none since there are going to be army inputs without support
        self.armyName = armyName
        self.location = location
        self.obj = obj
        self.supportLevel = 0   #Support level starts at 0, we will fill in later
        self.combat = False     #This is true if an player is engaging in combat (same location as another player)


def diplomacy_solve(r, w):

    armies = []
    for s in r:
        armies.append(setup(s))  

    warzone = []
    res = []

    move(warzone, armies)
    support(res, armies)
    res.extend(result(warzone)) #Extend i

    for i in sorted(res):
        #print(i)
        w.write(i + "\n")  #Outputing the results




def setup(s):
    input = s.split()

    if input [2] == "Move":
        player = Player(input[0], input[3])   #We set up army and destination
    elif input[2] == "Support":
        player = Player(input[0], input[1], input[3])  #We set up army, location, and who the army is supporting
    else:
        player = Player(input[0], input[1])   #Holding position

    return player
    

def support(res, armies):
    for player in armies:
        if (not player.combat):
            res.append(player.armyName + " " + player.location)
            if player.obj:
                for army2 in armies:
                    if army2.armyName == player.obj:    #Increase supportLevel if army is being supported by another army
                        army2.supportLevel += 1
                        break


def move(warzone, armies):
    for i in range(len(armies)):
        armyName = armies[i]
        battle = [armyName] 
        j = i + 1
        while j < len(armies):
            if armies[j].location == armyName.location:
                battle.append(armies[j])    #If two armies are in the same location, they fight
                armyName.combat, armies[j].combat = True, True  
            j += 1
        if len(battle) > 1: #War bound if there are multiple people in the 
            warzone.append(battle)



def result(warzone):
    outcome = []    #Store battle results here

    for battle in warzone:
        strongest = 0

        while len(battle) > 1:
            i = len(battle) - 1

            while i >= 0:
                if battle[i].supportLevel <= strongest: #If not the strongest army, they die
                    result = battle[i].armyName + " [dead]"
                    if result not in outcome:
                        outcome.append(result)
                    battle.pop(i)
                i -= 1 #Iterate backwards
            strongest += 1

        repeat = False
        if len(battle) > 0:
            result = battle[0].armyName + " " + battle[0].location
            for res in outcome:
                if result[0] == res[0]:
                    repeat = True
            outcome.append(result) 
            if repeat:
                outcome.pop(-1)
    return outcome 

    




#r = StringIO("C Houston Support B\nB Austin Move Dallas\nA Dallas Hold\n")
#w = StringIO()
#diplomacy_solve(r, w)

    
